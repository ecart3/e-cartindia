CREATE TABLE products (
    p_id bigserial PRIMARY KEY,
    product_availability boolean,
    brand_name varchar(255),
    product_desc varchar(255),
    image_name varchar(255),
    is_live boolean,
    product_name varchar(255),
    product_price bigint,
    stock_quantity bigint
);
