CREATE TABLE role (
    id BIGSERIAL PRIMARY KEY,
    role_name VARCHAR(50),
    description VARCHAR(255)
);

INSERT INTO ROLE(role_name,description)VALUES('ROLE_USER','This role is used for user'),
                                             ('ROLE_ADMIN','This role is used for admin user');