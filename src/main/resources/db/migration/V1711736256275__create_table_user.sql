CREATE TABLE users(
id BIGSERIAL PRIMARY KEY,
user_name VARCHAR(255),
password VARCHAR(255),
email VARCHAR(255) UNIQUE,
first_name VARCHAR(255),
last_name VARCHAR(255),
phone_Number VARCHAR(10),
gender VARCHAR(255),
account_satus Boolean
);
