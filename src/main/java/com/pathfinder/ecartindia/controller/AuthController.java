package com.pathfinder.ecartindia.controller;

import com.pathfinder.ecartindia.WebServiceException;
import com.pathfinder.ecartindia.jwt.JwtHelper;
import com.pathfinder.ecartindia.models.Users;
import com.pathfinder.ecartindia.repository.UserRepository;
import com.pathfinder.ecartindia.security.UserInfo;
import com.pathfinder.ecartindia.services.UserService;
import com.pathfinder.ecartindia.utility.constants.Constant;
import com.pathfinder.ecartindia.utility.dto.SignUpDto;
import com.pathfinder.ecartindia.utility.dto.UserDto;
import com.pathfinder.ecartindia.utility.response.Error;
import com.pathfinder.ecartindia.utility.response.JwtRequest;
import com.pathfinder.ecartindia.utility.response.JwtResponse;
import com.pathfinder.ecartindia.utility.response.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserInfo userDetailsService;

    @Autowired
    private AuthenticationManager manager;
    @Autowired
    private UserService userService;

    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    UserRepository userRepository;

  //  @Autowired
    //JwtResponse jwtResponse;

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody JwtRequest jwtRequest){
    //starting authenticating
        log.info("User loggedIn");
        try {
             JwtResponse response=  userService.login(jwtRequest);
            return ResponseEntity.ok(ResponseUtil.populateResponseObject(response,Constant.SUCCESS,null));
             }
        catch (BadCredentialsException ex){
      log.info("Invalid Usernaem and Password");
            return ResponseEntity.internalServerError().body(ResponseUtil.populateResponseObject("Invalid Usernaem and Password",Constant.FAILURE,new Error("login",ex.getMessage())));
        }
        catch (Exception ex){
        return ResponseEntity.internalServerError().body(ResponseUtil.populateResponseObject(null,Constant.FAILURE,new Error("login",ex.getMessage())));
        }

    }

}
