package com.pathfinder.ecartindia.controller;

import com.pathfinder.ecartindia.repository.UserRepository;
import com.pathfinder.ecartindia.services.UserService;
import com.pathfinder.ecartindia.utility.constants.Constant;
import com.pathfinder.ecartindia.utility.dto.SignUpDto;
import com.pathfinder.ecartindia.utility.dto.UserDto;
import com.pathfinder.ecartindia.utility.response.Error;
import com.pathfinder.ecartindia.utility.response.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;
    @PostMapping("/signup")
    public ResponseEntity<Object> signUp(@RequestBody UserDto userDto) {
        log.info("Inside UserController of signUp() method");
        try {
            if(userRepository.existsByemail(userDto.getEmail())){
                return ResponseEntity.ok(
                        ResponseUtil.populateResponseObject("User is already existed",Constant.FAILURE,null));
            }
            String signup = userService.signUp(userDto);
            return ResponseEntity.ok(
                    ResponseUtil.populateResponseObject(signup, Constant.SUCCESS, null));
        }catch (Exception ex){
            log.info("Inside catch block of Usercontroller signUp() method::");
            return ResponseEntity.internalServerError().
                    body(ResponseUtil.populateResponseObject(null, Constant.FAILURE,
                            new Error("",ex.getMessage())));
        }

    }

    @GetMapping("/get-by-id/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable("id") Long id){
    log.info("Inside UserController of getUserById() method::");
    try {
    UserDto userDto=userService.getUserByid(id);
    return ResponseEntity.ok(
            ResponseUtil.populateResponseObject(userDto,Constant.SUCCESS,null));
    }catch (Exception ex){
    log.info("Inside catch block of UserController getUserById() method::");
    return ResponseEntity.internalServerError().body(
            ResponseUtil.populateResponseObject(null,Constant.FAILURE,
                    new Error("getUserById",ex.getMessage())));
    }

    }
    @GetMapping("/get-all-user")
    public ResponseEntity<Object> getAllUser(){
     return null;
    }
    @PutMapping("/update-user/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable("id") Long id, @RequestBody UserDto userDto){
       log.info("Inside Usercontroller of updateUser() method::");
        try {
           userService.updateUserById(id,userDto);
           return ResponseEntity.ok(ResponseUtil.populateResponseObject(
                   "User updated successfully",Constant.SUCCESS,null));
        }catch (Exception ex){
        return ResponseEntity.internalServerError().body(
                ResponseUtil.populateResponseObject(null,Constant.FAILURE,new Error("updateUser",ex.getMessage())));
        }
    }
    @DeleteMapping("/delete-user-by-id/{id}")
    public ResponseEntity<Object> deleteUserById(@PathVariable("id") Long id){
    log.info("Inside User Controller of deleteUserById():: method");
    try {
    userService.deleteUserByid(id);
    return ResponseEntity.ok(ResponseUtil.populateResponseObject(
            "User Deleted Successfully"  ,Constant.SUCCESS,null));
    }catch (Exception ex){
    //return ResponseEntity.internalServerError().body(Respon)

    }
        return null;
    }
}
