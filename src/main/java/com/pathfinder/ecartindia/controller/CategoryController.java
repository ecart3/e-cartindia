package com.pathfinder.ecartindia.controller;

import com.pathfinder.ecartindia.services.CategoryService;
import com.pathfinder.ecartindia.utility.constants.Constant;
import com.pathfinder.ecartindia.utility.dto.CategoryDto;
import com.pathfinder.ecartindia.utility.response.Error;
import com.pathfinder.ecartindia.utility.response.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/category")
@Slf4j
public class CategoryController {

    @Autowired
    public CategoryService categoryService;
    @PostMapping("/create-category")
    public ResponseEntity<Object> createCategory(@RequestBody CategoryDto categoryDto) {
        log.info("Inside create-category Controller of method createCategory :::");
        try {
            String saveCategroy = categoryService.createCategory(categoryDto);
            return ResponseEntity.ok(ResponseUtil.populateResponseObject(saveCategroy, Constant.SUCCESS, null));
        } catch (Exception ex) {
            log.info("Getting issue to save category" + ex.getMessage());
            return ResponseEntity.internalServerError().body(ResponseUtil.
                    populateResponseObject(null, Constant.FAILURE,
                            new Error("createCategory", ex.getMessage())));
        }
}
    public ResponseEntity<Object> getAllCategory(){
        return null;
    }

    @GetMapping("/get-by-id/{id}")
    public ResponseEntity<Object> getCategoryById(@PathVariable("id") Long id){
        log.info("Inside category controller getCategoryById() method::");
        try {
            CategoryDto categoryDto=categoryService.getCategoryById(id);
            return ResponseEntity.ok(
                    ResponseUtil.populateResponseObject(categoryDto,Constant.SUCCESS,null));
        }catch (Exception ex){
        log.info("Inside catch block of CategoryController getCategoryById() method::");
       return ResponseEntity.internalServerError().body(ResponseUtil.populateResponseObject(
               null,Constant.FAILURE,new Error("Category", ex.getMessage())));
        }
    }
    @DeleteMapping("/delete-by-id/{id}")
    public ResponseEntity<Object> deleteCategorgyByid(@PathVariable("id") Long id){
        log.info("Inside category controller of deleteCategoryByid() method::");
        try{
        categoryService.deleteCategoryByid(id);
        return ResponseEntity.ok(
                ResponseUtil.populateResponseObject("Category Deleted Successfully",Constant.SUCCESS,null));
        }catch (Exception ex){
        log.info("Inside catch block of Category Controller deleteCategoryByid() method"+ex.getMessage());
        return ResponseEntity.internalServerError().body(ResponseUtil.populateResponseObject(null,Constant.FAILURE,
                new Error("deleteCategorgyByid",ex.getMessage())));
        }

    }
    @PutMapping("/update-category/{id}")
    public ResponseEntity<Object> updateCategoryById(@PathVariable("id") Long id, @RequestBody CategoryDto categoryDto){
    log.info("inside category controler of updateCategoryById() method");
    try {
        String message=categoryService.updateCategoryById(categoryDto,id);
        return ResponseEntity.ok(
                ResponseUtil.populateResponseObject(message,Constant.SUCCESS,null));
    }catch (Exception ex){
        return ResponseEntity.internalServerError().body(
                ResponseUtil.populateResponseObject(null,Constant.FAILURE,new Error("updateCategoryById",ex.getMessage())));
    }
    }
}
