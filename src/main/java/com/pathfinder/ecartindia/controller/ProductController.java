package com.pathfinder.ecartindia.controller;
import com.pathfinder.ecartindia.models.Product;
import com.pathfinder.ecartindia.services.ProductService;
import com.pathfinder.ecartindia.utility.constants.Constant;
import com.pathfinder.ecartindia.utility.dto.ProductDto;
import com.pathfinder.ecartindia.utility.response.Error;
import com.pathfinder.ecartindia.utility.response.PageableResponse;
import com.pathfinder.ecartindia.utility.response.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/products")
public class ProductController {
    @Autowired
    ProductService productService;


    //@PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/welcome")
    public String welcome(){
        return "welcome";
    }
    @PostMapping("/create-product/{categoryid}")
    public ResponseEntity<Object> creatProducts(@RequestBody ProductDto productDto, @PathVariable("categoryid") Long categoryid) {
        try {
            log.info("Inside Products Controller of createProducts method :::");
            String produtcId = productService.createProduct(productDto,categoryid);
            return ResponseEntity.ok(ResponseUtil.populateResponseObject(produtcId,
                    Constant.SUCCESS, null));
        } catch (Exception ex) {
            return ResponseEntity.internalServerError().body(
                    ResponseUtil.populateResponseObject(null,
                            Constant.FAILURE, new Error("creatProducts", ex.getMessage())));
        }
    }


    @PutMapping("/update-product/{pid}")
    public ResponseEntity<Object> updateProduct(@PathVariable("pid") Long pid, @RequestBody ProductDto productDto) {
        try {
            log.info("inside productController of updateProduct() method::");
            String updateProduct = productService.updateProduct(productDto, pid);
            return ResponseEntity.ok(ResponseUtil.populateResponseObject(updateProduct, Constant.SUCCESS, null));
        } catch (Exception ex) {
            return ResponseEntity.internalServerError().body(ResponseUtil.populateResponseObject(null, Constant.FAILURE,
                    new Error("updateProduct", ex.getMessage())));
        }
    }

    @DeleteMapping("/delete-by-id/{pid}")
    public ResponseEntity<Object> deleteProductById(@PathVariable("pid") Long pid){
        try{
            productService.deleteProductById(pid);
            return ResponseEntity.ok(
                    ResponseUtil.populateResponseObject("deleted successfully",Constant.SUCCESS,null));
        }catch (Exception ex){
            return ResponseEntity.internalServerError().body(ResponseUtil.populateResponseObject(null,Constant.FAILURE,new Error("deleteProductById",ex.getMessage())));
        }
    }
    @GetMapping("/get-all")
    public ResponseEntity<Object> getAllProducts(@RequestParam(value = "page",defaultValue = "0") Integer page,
                                                @RequestParam(value="size", defaultValue="10") Integer size,
                                                 @RequestParam(value = "sortBy", defaultValue = "pId", required=false) String sortBy,
                                                 @RequestParam(value="direction", defaultValue = "desc", required=false) String direction){
try{
    Pageable pageable=null;
    if(sortBy.equalsIgnoreCase(sortBy)){
        sortBy="pId";
    }
    if (direction.equalsIgnoreCase("desc")){
        pageable= PageRequest.of(page,size, Sort.by(Sort.Order.desc(sortBy)));
    }else{
        pageable=PageRequest.of(page,size,Sort.by(Sort.Order.asc(sortBy)));
    }
  PageableResponse<List<ProductDto>> list= productService.getAll(pageable);
    return ResponseEntity.ok(
            ResponseUtil.populateResponseObject(list,"SUCCESS",null));

}catch (Exception ex){
    return ResponseEntity.internalServerError().
            body(ResponseUtil.populateResponseObject(null,Constant.FAILURE,new Error("getAllProducts",ex.getMessage())));
}
           }

   // @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/get-by-id/{id}")
    public ResponseEntity<Object> getById(@PathVariable("id") Long id) {
        try {
            log.info("inside productController of getById() method::");
            ProductDto product = productService.getByProductId(id);
            return ResponseEntity.ok(ResponseUtil.populateResponseObject(product, Constant.SUCCESS, null));
        } catch (Exception ex) {
            log.info("inside catch block of productController of getById() method::");
            return ResponseEntity.internalServerError().
                    body(ResponseUtil.populateResponseObject(null, Constant.FAILURE,
                            new Error("getById", ex.getMessage())));
        }
    }
}
