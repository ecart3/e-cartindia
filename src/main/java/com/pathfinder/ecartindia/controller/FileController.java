package com.pathfinder.ecartindia.controller;

import com.pathfinder.ecartindia.services.FileUploadService;
import com.pathfinder.ecartindia.utility.constants.Constant;
import com.pathfinder.ecartindia.utility.response.Error;
import com.pathfinder.ecartindia.utility.response.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@Slf4j
@RequestMapping("/file")
public class FileController {

    @Autowired
    private FileUploadService fileUploadService;

    @PostMapping("/upload-file")
    public ResponseEntity<Object> uploadFile(@RequestParam MultipartFile multipartFile) throws IOException {
      log.info("inside file controller of uploadFile()::");
      try{
          fileUploadService.uploadFile(multipartFile);
          return ResponseEntity.ok(ResponseUtil.populateResponseObject("File Uploaded Successfully", Constant.SUCCESS,null));
      }catch (Exception ex){
          log.info("inside catch block of File controller uploadFile() method::");
          return ResponseEntity.ok(ResponseUtil.populateResponseObject(null,Constant.FAILURE,new Error("uploadFile",ex.getMessage())));
      }
    }
    @DeleteMapping("/file-delete/{id}")
    public ResponseEntity<Object> deleteFileFromS3(@PathVariable("id") Long id){
        log.info("inside file controller of deleteFileFromS3() method::");
        try{
        fileUploadService.deleteFileFromS3(id);
        return ResponseEntity.ok(ResponseUtil.populateResponseObject("Deleted Successfully",Constant.SUCCESS,null));
        }catch (Exception ex){
        return ResponseEntity.internalServerError().body(ResponseUtil.populateResponseObject(null,Constant.FAILURE,new Error("Delete File",ex.getMessage())));
        }
    }

    //File Donwload Controller

    @PostMapping("/downloadFile")
    public ResponseEntity<Object> downloadFileName(){
        log.info("inside FileController of downloadFileName() method::");
         try {
             fileUploadService.downloadFile();
             return ResponseEntity.ok(ResponseUtil.populateResponseObject(
                     "Downloaded Successfully",Constant.SUCCESS,null));
         }catch (Exception ex){
            return ResponseEntity.internalServerError().body(
                    ResponseUtil.populateResponseObject(null,Constant.FAILURE,new Error("downloadFileName",ex.getMessage())));
         }
    }
}
