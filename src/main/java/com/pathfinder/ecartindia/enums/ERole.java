package com.pathfinder.ecartindia.enums;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
