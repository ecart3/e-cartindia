package com.pathfinder.ecartindia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class ECartIndiaApplication {

	public static void main(String[] args) {

		SpringApplication.run(ECartIndiaApplication.class, args);

//		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//
//		// Encode the password
//		String encodedPassword = encoder.encode("monu");
//		System.out.println(encodedPassword);
	}


}
