package com.pathfinder.ecartindia.services;

import com.pathfinder.ecartindia.WebServiceException;
import com.pathfinder.ecartindia.enums.ERole;
import com.pathfinder.ecartindia.jwt.JwtHelper;
import com.pathfinder.ecartindia.models.Role;
import com.pathfinder.ecartindia.models.Users;
import com.pathfinder.ecartindia.repository.RoleRepository;
import com.pathfinder.ecartindia.repository.UserRepository;
import com.pathfinder.ecartindia.security.UserInfo;
import com.pathfinder.ecartindia.utility.constants.Constant;
import com.pathfinder.ecartindia.utility.dto.ActorEntity;
import com.pathfinder.ecartindia.utility.dto.UserDto;
import com.pathfinder.ecartindia.utility.response.JwtRequest;
import com.pathfinder.ecartindia.utility.response.JwtResponse;
import com.pathfinder.ecartindia.utility.response.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Service
@Slf4j
public class UserServiceImpl implements UserService{

    @Autowired
    public UserRepository userRepository;
    @Autowired
    public RoleRepository roleRepository;

    @Autowired
    private JwtHelper jwtHelper;

    @Autowired
    private AuthenticationManager manager;

    @Autowired
    private UserInfo userDetailsService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Override
    public String signUp(UserDto userDto) throws WebServiceException {


    Users user=new Users();
    user.setUserName(userDto.getEmail());
    user.setAccountStatus(userDto.getAccountStatus());
    user.setGender(userDto.getGender());
    user.setEmail(userDto.getEmail());
    user.setFirstName(userDto.getFirstName());
    user.setLastName(userDto.getLastName());
    user.setPhoneNumber(userDto.getPhoneNumber());
    //set createdby date
        ActorEntity actor=new ActorEntity(userDto.getEmail());
        user.setCreatedBy(actor);
        user.setUpdatedBy(actor);
    String encodedPassword = passwordEncoder.encode(userDto.getPassword());
    user.setPassword(encodedPassword);
    Set<String> roles=userDto.getRoles();
    Set<Role> role=new HashSet<>();
    if(roles==null){
        Role roless=roleRepository.findByName(ERole.ROLE_USER);
        role.add(roless);
    }
    user.setRoles(role);
    Users user1=userRepository.save(user);
    return ("User is registered successfully with this id"+user1.getId());
    }

    @Override
    public JwtResponse login(JwtRequest jwtRequest) throws WebServiceException {

        UsernamePasswordAuthenticationToken authenticates=new UsernamePasswordAuthenticationToken(jwtRequest.getUsername(),jwtRequest.getPassword());
        Authentication auth= manager.authenticate(authenticates);
        UserDetails userDetails= userDetailsService.loadUserByUsername(jwtRequest.getUsername());
        String token=this.jwtHelper.generateToken(userDetails);
        JwtResponse response=JwtResponse.builder().jwtToken(token).username(userDetails.getUsername()).build();
        return response;
    }

    @Override
    public UserDto getUserByid(Long id) throws WebServiceException {
        log.info("Inside UserServiceImpl of getUserByid() method");
       Users user= userRepository.findById(id).orElseThrow(()->new WebServiceException("User id is not found"));
        UserDto userDto=null;
       if(!ObjectUtils.isEmpty(user) && Objects.nonNull(user)){
           userDto=new UserDto();
           userDto.setId(user.getId());
           userDto.setUserName(user.getUserName());
           userDto.setAccountStatus(user.getAccountStatus());
           userDto.setCreatedBy(user.getCreatedBy());
           userDto.setUpdatedBy(user.getUpdatedBy());
           userDto.setEmail(user.getEmail());
           userDto.setFirstName(user.getFirstName());
           userDto.setLastName(user.getLastName());
           userDto.setPhoneNumber(user.getPhoneNumber());
           userDto.setGender(user.getGender());
           return userDto;
       }
       return userDto;
    }

    @Override
    public void updateUserById(Long id,UserDto userDto) throws WebServiceException {
        if(Objects.isNull(userDto) && ObjectUtils.isEmpty(userDto))
            throw new WebServiceException("UserDto is empty");

        Users user=userRepository.findById(id).orElseThrow(
                ()->new WebServiceException("User id is not found"));
try {

//    user.setUserName(userDto.getUserName());
//    user.setAccountStatus(userDto.getAccountStatus());

   // user.setCreatedBy(userDto.getCreatedBy());
    //user.setUpdatedBy(userDto.getUpdatedBy());
    //user.setEmail(userDto.getEmail());
//    user.setFirstName(userDto.getFirstName());
//    user.setLastName(userDto.getLastName());
//    user.setPhoneNumber(userDto.getPhoneNumber());
//    user.setGender(userDto.getGender());
    userRepository.save(user);
}catch (Exception ex){
    throw  new WebServiceException("Getting Error while updating user details");
}
    }

    @Override
    public void deleteUserByid(Long id) throws WebServiceException {
       Users users= userRepository.findById(id).orElseThrow(()->new WebServiceException("User is not found"));
        userRepository.delete(users);
    }
}
