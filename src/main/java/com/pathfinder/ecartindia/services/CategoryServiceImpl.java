package com.pathfinder.ecartindia.services;

import com.pathfinder.ecartindia.WebServiceException;
import com.pathfinder.ecartindia.models.Category;
import com.pathfinder.ecartindia.repository.CategoryRepository;
import com.pathfinder.ecartindia.utility.dto.CategoryDto;
import com.pathfinder.ecartindia.utility.dto.ProductDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    public CategoryRepository categoryRepository;

    @Override
    public String createCategory(CategoryDto categoryDto) throws WebServiceException {
        try {
            log.info("Inside category service to save category::");
            Category category = new Category();
            category.setName(categoryDto.getName());
            categoryRepository.save(category);
            return "category is created successfully with this id" + category.getId();

        } catch (Exception ex) {
            log.error("Error occurred while saving the category: {}", ex.getMessage());
            throw new WebServiceException("getting error to saved the category" + ex.getMessage());
        }
    }

    @Override
    public CategoryDto getCategoryById(Long id) throws WebServiceException {
        Category category = categoryRepository.findById(id).orElseThrow(
                () -> new WebServiceException("Category id is not found"));
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName(category.getName());
        categoryDto.setId(category.getId());
        return categoryDto;
    }

    @Override
    public void deleteCategoryByid(Long id) throws WebServiceException {
        Category category = categoryRepository.findById(id).orElseThrow(
                () -> new WebServiceException("Category id is not found"));
        try {
            categoryRepository.delete(category);
        } catch (Exception ex) {
            throw new WebServiceException("Getting error to delete category");
        }

    }

    @Override
    public String updateCategoryById(CategoryDto categoryDto, Long id) throws WebServiceException {
       Category category= categoryRepository.findById(id).orElseThrow(
                ()->new WebServiceException("category id is not found"));
        CategoryDto categoryDto1=new CategoryDto();
        category.setName(categoryDto.getName());
        categoryRepository.save(category);
        return "Category is updated successfully";
    }
}
