package com.pathfinder.ecartindia.services;

import com.pathfinder.ecartindia.WebServiceException;
import com.pathfinder.ecartindia.utility.dto.SignUpDto;
import com.pathfinder.ecartindia.utility.dto.UserDto;
import com.pathfinder.ecartindia.utility.response.JwtRequest;
import com.pathfinder.ecartindia.utility.response.JwtResponse;

public interface UserService {

    String signUp(UserDto userDto) throws WebServiceException;
    JwtResponse login(JwtRequest request) throws WebServiceException;

    UserDto getUserByid(Long id) throws WebServiceException;

    void updateUserById(Long id,UserDto userDto) throws WebServiceException;

    void deleteUserByid(Long id) throws WebServiceException;

}
