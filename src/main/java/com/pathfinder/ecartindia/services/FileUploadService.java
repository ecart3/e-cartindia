package com.pathfinder.ecartindia.services;

import com.pathfinder.ecartindia.WebServiceException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileUploadService {

    void uploadFile(MultipartFile multipartFile) throws WebServiceException, IOException;
    void deleteFileFromS3(Long id) throws WebServiceException;
    void downloadFile() throws WebServiceException;
}
