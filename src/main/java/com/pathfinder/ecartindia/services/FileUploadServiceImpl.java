package com.pathfinder.ecartindia.services;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.pathfinder.ecartindia.WebServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
@Slf4j
public class FileUploadServiceImpl implements FileUploadService{

    @Value("${aws.bucketName}")
    private String bucketName;
    @Value("${aws.accessKey}")
    private String accessKey;
    @Value("${aws.secretKey}")
    private String secretKey;

    @Value("${aws.region}")
    private String awsRegion;
    @Value("${directory1.path}")
    private String fileLocation;
//    @Autowired
//    private AmazonS3 amazonS3;

    @Override
    public void uploadFile(MultipartFile multipartFile) throws WebServiceException, IOException {
        AWSCredentials credentials= new BasicAWSCredentials(accessKey, secretKey);
        AmazonS3 amazonS3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(awsRegion).build();
        amazonS3.putObject(bucketName, multipartFile.getOriginalFilename(),multipartFile.getInputStream(),null);

    }

    @Override
    public void deleteFileFromS3(Long id) throws WebServiceException {
        AWSCredentials credentials= new BasicAWSCredentials(accessKey, secretKey);
        AmazonS3 amazonS3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(awsRegion).build();
        String fileName="UPDATED_Components - Quotation Format 3 (1).xlsx";
        log.info("File deleted successfully");
        amazonS3.deleteObject(bucketName, fileName);
    }

    @Override
    public void downloadFile() throws WebServiceException {
    try{
       // need to give filename;
        String fileName="UPDATED_Components - Quotation Format 3 (1).xlsx";;
        AWSCredentials credentials= new BasicAWSCredentials(accessKey, secretKey);
        AmazonS3 amazonS3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(awsRegion).build();

        S3Object s3Object = amazonS3.getObject(new GetObjectRequest(bucketName, fileName));
        File file = new File(fileLocation);
        FileOutputStream outputStream = new FileOutputStream(file);
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = s3Object.getObjectContent().read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        outputStream.close();
        s3Object.close();
        System.out.println("File downloaded successfully.");
    }catch (Exception ex){
        ex.printStackTrace();
    }
    }
}
