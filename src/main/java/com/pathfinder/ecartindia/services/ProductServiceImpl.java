package com.pathfinder.ecartindia.services;

import com.pathfinder.ecartindia.WebServiceException;
import com.pathfinder.ecartindia.models.Category;
import com.pathfinder.ecartindia.models.Product;
import com.pathfinder.ecartindia.repository.CategoryRepository;
import com.pathfinder.ecartindia.repository.ProductRepository;
import com.pathfinder.ecartindia.utility.dto.CategoryDto;
import com.pathfinder.ecartindia.utility.dto.ProductDto;
import com.pathfinder.ecartindia.utility.response.PageableResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;


    @Override
    public PageableResponse<List<ProductDto>> getAll(Pageable pageable) throws WebServiceException {
     try {
         Page<Product> productPage=productRepository.findAll(pageable);
         List<Product> prod=new ArrayList<>();
         if(productPage!=null && productPage.getTotalElements()>0){
             prod= productPage.getContent();
         }
         List<ProductDto> listOfProduct=new ArrayList<>();
         prod.forEach(prods->{
             ProductDto productDto=new ProductDto();
             productDto.setName(prods.getName());
             productDto.setDescription(prods.getDescription());
             productDto.setBrand(prods.getBrand());
             productDto.setPrice(prods.getPrice());
             productDto.setIsLive(prods.getIsLive());
             productDto.setImageName(prods.getImageName());
             productDto.setStockQuantity(prods.getStockQuantity());
             //set Category
             CategoryDto categoryDto=new CategoryDto();
             categoryDto.setId(prods.getCategory().getId());
             categoryDto.setName(prods.getCategory().getName());
             productDto.setCategoryDto(categoryDto);
             listOfProduct.add(productDto);
         });
         return new PageableResponse<>(productPage,listOfProduct);
     }catch (Exception ex){
         ex.printStackTrace();
         throw new WebServiceException("Exception Occured while getting getAll products"+ex.getMessage());
     }
    }
    @Override
    public String createProduct(ProductDto productDto, Long categoryId) throws WebServiceException {
        Category category= categoryRepository.findById(categoryId).orElseThrow(()->new WebServiceException("Category id is not found"+categoryId));
        try {
            Product product = new Product();
            if (Objects.isNull(productDto) && ObjectUtils.isEmpty(productDto))
                throw new WebServiceException("ProductDto is not found");
            Product products = setProductData(productDto,category);
            return "Product is created successfully with this id::"+products.getPId();
        } catch (Exception ex) {
            log.info("inside createProduct() service "+ex.getMessage());
            throw new WebServiceException("Getting troubeled to create product" + ex.getMessage());
        }
    }


    public Product setProductData(ProductDto productDto, Category category) {
        Product product = new Product();
        product.setName(productDto.getName());
        product.setBrand(productDto.getBrand());
        product.setAvaibility(false);
        product.setStockQuantity(productDto.getStockQuantity());
        product.setPrice(productDto.getPrice());
        product.setIsLive(false);
        product.setIsLive(productDto.getIsLive());
        product.setImageName(productDto.getImageName());
        product.setDescription(productDto.getDescription());
        product.setCategory(category);
        Product products = productRepository.save(product);
        return products;
    }


    @Override
    public ProductDto getByProductId(Long productId) throws WebServiceException {
        Product product=productRepository.findById(productId).orElseThrow(()->
                new WebServiceException("productId is not found"));
        ProductDto productDto=new ProductDto();
        productDto.setName(product.getName());
        productDto.setBrand(product.getBrand());
        productDto.setAvaibility(product.getAvaibility());
        productDto.setStockQuantity(product.getStockQuantity());
        productDto.setPrice(product.getPrice());
        productDto.setIsLive(product.getIsLive());
        productDto.setImageName(product.getImageName());
        productDto.setDescription(product.getDescription());
        //set category dto
        CategoryDto categoryDto=new CategoryDto();
        categoryDto.setId(product.getCategory().getId());
        categoryDto.setName(product.getCategory().getName());
        productDto.setCategoryDto(categoryDto);

        return productDto;
    }

    @Override
    public String updateProduct(ProductDto productDto,Long id) throws WebServiceException {
       Product product= productRepository.findById(id).orElseThrow(()-> new WebServiceException("Product id is not found for update"));
       if(ObjectUtils.isEmpty(productDto) && Objects.isNull(productDto))
           throw new WebServiceException("productDto is not found");

        product.setName(productDto.getName());
        product.setBrand(productDto.getBrand());
        product.setAvaibility(productDto.getAvaibility());
        product.setStockQuantity(productDto.getStockQuantity());
        product.setPrice(productDto.getPrice());
        product.setIsLive(productDto.getIsLive());
        product.setIsLive(productDto.getIsLive());
        product.setImageName(productDto.getImageName());
        product.setDescription(productDto.getDescription());
        Product produc= productRepository.save(product);
        return "Product is updated successully::"+produc.getPId();
    }

    @Override
    public void deleteProductById(Long id) throws WebServiceException {
        if(ObjectUtils.isEmpty(id))
            throw new WebServiceException("id is not found");

        Product product= productRepository.findById(id).orElseThrow(()->
               new WebServiceException("Product id is not found for delete"));
       productRepository.delete(product);
    }




}