package com.pathfinder.ecartindia.services;

import com.pathfinder.ecartindia.WebServiceException;
import com.pathfinder.ecartindia.utility.dto.ProductDto;
import com.pathfinder.ecartindia.utility.response.PageableResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService {
    String createProduct(ProductDto productDto, Long categoryId) throws WebServiceException;
    ProductDto getByProductId(Long productId) throws WebServiceException;
    String updateProduct(ProductDto productDto,Long id) throws WebServiceException;

    void deleteProductById(Long id) throws WebServiceException;
    PageableResponse<List<ProductDto>> getAll(Pageable pageable) throws WebServiceException;



}
