package com.pathfinder.ecartindia.services;

import com.pathfinder.ecartindia.WebServiceException;
import com.pathfinder.ecartindia.utility.dto.CategoryDto;
import com.pathfinder.ecartindia.utility.dto.ProductDto;

public interface CategoryService {
    String createCategory(CategoryDto CategoryDto) throws WebServiceException;
    CategoryDto getCategoryById(Long id) throws WebServiceException;

    void deleteCategoryByid(Long id) throws WebServiceException;

    String updateCategoryById(CategoryDto categoryDto,Long id) throws WebServiceException;

}
