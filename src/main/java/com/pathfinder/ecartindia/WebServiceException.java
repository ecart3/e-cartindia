package com.pathfinder.ecartindia;

public class WebServiceException extends RuntimeException{

    public WebServiceException(){
        super();
    }
    public WebServiceException(String message){
        super(message);
    }
}
