package com.pathfinder.ecartindia.repository;

import com.pathfinder.ecartindia.WebServiceException;
import com.pathfinder.ecartindia.models.Users;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users,Long> {
    boolean existsByemail(String email) throws WebServiceException;
    Optional<Users> findByuserName(String username) throws WebServiceException;
}
