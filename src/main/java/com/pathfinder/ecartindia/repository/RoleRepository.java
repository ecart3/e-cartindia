package com.pathfinder.ecartindia.repository;

import com.pathfinder.ecartindia.enums.ERole;
import com.pathfinder.ecartindia.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    Role findByName(ERole status);
}
