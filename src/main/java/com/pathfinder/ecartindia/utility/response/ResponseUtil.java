package com.pathfinder.ecartindia.utility.response;

public class ResponseUtil {

    public static ResponseObject<Object> populateResponseObject(final Object responseObject,
                                                         final String responeStatus, final Error error){
        ResponseObject<Object> response=new ResponseObject();
        response.setResponse(responseObject);
        Status stats=new Status();
        stats.setResponseStatus(responeStatus);
        stats.setError(error);
        response.setStatus(stats);
        return response;
    }

}
