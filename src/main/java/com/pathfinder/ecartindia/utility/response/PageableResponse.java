package com.pathfinder.ecartindia.utility.response;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

@Getter
@Setter
public class PageableResponse<T> {
    private Long totalElements;
    private Integer totalPages;
    private Integer size;
    private Integer page;
    private Integer numberOfElements;
    private Object list;
    private String lastEvaluateKey;
    public PageableResponse(){}
    public PageableResponse(Page<?> page, T content){
        if(content==null){
            this.list=page.getContent();
        }else{
            this.list=content;
            this.totalElements=page.getTotalElements();
            this.totalPages= page.getTotalPages();
            this.size=page.getSize();
            this.page=page.getNumber();
            this.numberOfElements=page.getNumberOfElements();
        }
    }
}
