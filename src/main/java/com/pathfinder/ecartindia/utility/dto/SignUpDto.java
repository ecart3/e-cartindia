package com.pathfinder.ecartindia.utility.dto;

import jakarta.persistence.Column;
import lombok.Data;

import java.util.Date;

@Data
public class SignUpDto {

    private String userName;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String gender;
    private Boolean accountStatus;
    private Date dob;

}
