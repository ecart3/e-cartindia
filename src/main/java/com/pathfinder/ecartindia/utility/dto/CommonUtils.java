package com.pathfinder.ecartindia.utility.dto;


import org.springframework.stereotype.Component;


@Component
public class CommonUtils {

    private static ThreadLocal<String> currentUserName = new InheritableThreadLocal<>();
    private static ThreadLocal<String> currentRole = new InheritableThreadLocal<>();
    private static ThreadLocal<String> currentEmail = new InheritableThreadLocal<>();

    public static final void setCurrentUserName(String userName){
    currentUserName.set(userName);
    }
    public static final void setCurrentRole(String role){
        currentRole.set(role);
    }
    public static final void setCurrentEmail(String email){
        currentEmail.set(email);
    }
    public static final String getCurrentUserName(){
    return currentUserName.get();
    }

    public static final String getCurrentEmail(){
        return currentEmail.get();
    }

    public static final String getCurrentRole(String role){
        return currentRole.get();
    }

    public static void setUserDetails(String username){
        System.out.println("Common Utils"+username);
      currentUserName.set(username);
//        currentEmail.set(email);
//        currentRole.set(role);
    }
}