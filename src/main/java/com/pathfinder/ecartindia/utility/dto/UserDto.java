package com.pathfinder.ecartindia.utility.dto;

import com.pathfinder.ecartindia.models.Cart;
import com.pathfinder.ecartindia.models.Role;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;
import java.util.Set;

@Data
public class UserDto {

    private Long id;
    private String userName;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String gender;
    private Boolean accountStatus;
    private Set<String> roles;
    private ActorEntity createdBy;
    private ActorEntity updatedBy;

    // private Cart cart;
}
