package com.pathfinder.ecartindia.utility.dto;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Embeddable
@NoArgsConstructor
public class ActorEntity {
    @Column(insertable=false, updatable=false)
    private String userEmailValue;
    public ActorEntity(String userEmailValue){
        this.userEmailValue=userEmailValue;
    }
}
