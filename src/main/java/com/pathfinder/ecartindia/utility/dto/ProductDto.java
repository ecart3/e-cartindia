package com.pathfinder.ecartindia.utility.dto;

import com.pathfinder.ecartindia.models.Category;
import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ProductDto {
        private String name;
        private String description;
        private Long price;
        private Long stockQuantity;
        private Boolean isLive;
        private String brand;
        private Boolean avaibility;
        private String imageName;
        private String productDisc;
        private CategoryDto categoryDto;
        private Date createdOn;
        private Date updatedOn;
        private Date createdBy;
        private Date updatedBy;
}
