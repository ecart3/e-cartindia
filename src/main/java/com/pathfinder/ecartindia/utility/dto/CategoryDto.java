package com.pathfinder.ecartindia.utility.dto;

import lombok.Data;

import java.util.Date;

@Data
public class CategoryDto {
    private Long id;
    private String name;
    private Date createdOn;
    private Date updatedOn;
    private Date createdBy;
    private Date updatedBy;
}
