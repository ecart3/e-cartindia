package com.pathfinder.ecartindia.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtHelper {

    public static final long JWT_TOKEN_VALIDITY=5 * 60 * 60;
    //public static final long JWT_TOKEN_VALIDITY=1 * 15 * 15;

    public static final String secret = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9eyJpZCI6IjVjOWYzYWI2NzY2Mjg2NDYyNDY0YTczNCIsIm5hbWUiOiJSYW5keSIsImF2YXRhciI6Ii8vd3d3LmdyYXZhdGFyLmNvbS9hdmF0YXIvMTNhN2MyYzdkOGVkNTNkMDc2MzRkOGNlZWVkZjM0NTEcz0yMDAmcj1wZyZkPW1tIiwiaWF0IjoxNTU0NTIxNjk1LCJleHAiOjE1NTQ1MjUyOTV9SxRurShXSSI3SE11z6nme9EoaD29TDBFr8Qwngkgmno";


//retrieve username from token
public String getUsernameFromToken(String token){
    return getClaimFromToken(token, Claims::getSubject);
}
//retrieve expiration date from jwttoken
public Date getExpirationDateFromTokne(String token){
    return getClaimFromToken(token, Claims::getExpiration);
}

public <T> T getClaimFromToken(String token,Function<Claims,T> claimsResolver){
    final Claims claims=getAllClaimsFromToken(token);
    return claimsResolver.apply(claims);
}
//for retrieve any information from token will need secret key
private Claims getAllClaimsFromToken(String token){
    return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
}

private Boolean isTokenExpired(String token){
  final Date expiration=getExpirationDateFromTokne(token);
  return expiration.before(new Date());
}

public Boolean isTokenExpireds(String token){
   return isTokenExpired(token);
}

//generate token for user
    public String generateToken(UserDetails userDetails){
    Map<String,Object> claims=new HashMap<>();
    return doGenerateToken(claims,userDetails.getUsername());
    }

    //while creating the token
    //Sign the JWT using the HS512 algorithm and secret key
    //According to JWS Compact Serialization
    //compaction of the jwt to a URL -safe String

    private String doGenerateToken(Map<String,Object> claims, String subject){
    return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis()+JWT_TOKEN_VALIDITY * 1000))

           .signWith(SignatureAlgorithm.HS512,secret).compact();
            //.signWith(getSignKey(),SignatureAlgorithm.HS256).compact();
}

//validitytoken
    public Boolean validityToken(String token, UserDetails userDetails){
    final String username=getUsernameFromToken(token);
    return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
}
