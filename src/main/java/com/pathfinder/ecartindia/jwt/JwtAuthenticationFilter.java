package com.pathfinder.ecartindia.jwt;

import com.google.gson.JsonObject;
import com.pathfinder.ecartindia.WebServiceException;
import com.pathfinder.ecartindia.security.UserInfo;
import com.pathfinder.ecartindia.utility.dto.CommonUtils;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Base64;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private Logger logger=LoggerFactory.getLogger(OncePerRequestFilter.class);

    @Autowired
    private JwtHelper jwtHelper;

    @Autowired
    private UserInfo userDetailsService;

    @Autowired
    CommonUtils commonUtils;

    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
    String requestHeader=request.getHeader("Authorization");
    logger.info("Header: {}"+requestHeader);

    String username=null;
    String token=null;
    if(requestHeader!=null && requestHeader.startsWith("Bearer ")) {
        //looking good
        try {
        token = requestHeader.substring(7,requestHeader.length());

        //extracting payload from token
            String[] jwtPart=token.split("\\.");
            String payload = decode(jwtPart[1]);
            JSONObject json=new JSONObject(payload);
            commonUtils.setUserDetails(json.getString("sub"));

            System.out.println("String value in jwt"+json.getString("sub"));

        //end extracting payload from token
            username = this.jwtHelper.getUsernameFromToken(token);




        } catch (IllegalArgumentException e) {
            logger.info("Illegal Argument while fetching the username");
           throw new WebServiceException("Illegal Argument while fetching the username");
        } catch (ExpiredJwtException e) {
            logger.info("Given Jwt token has expired");
            throw new WebServiceException("Given Jwt token has expired");
            //e.printStackTrace();
        } catch (MalformedJwtException e) {
            logger.info("Some changes has done in token !! Invalid token");
            throw new WebServiceException("Some changes has done in token !! Invalid token");
        }
    }else {
        logger.info("Invalid Header value");
    }

    if(username!=null && SecurityContextHolder.getContext().getAuthentication()==null){

        //fetch userdetail from username
        UserDetails userDetails= this.userDetailsService.loadUserByUsername(username);
        Boolean validateToken=this.jwtHelper.validityToken(token,userDetails);
        if(validateToken){
            //set the authentication
            UsernamePasswordAuthenticationToken authentication=new
                    UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
       authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
       SecurityContextHolder.getContext().setAuthentication(authentication);
        }else {
            logger.info("validation fails!!");
        }

    }


        filterChain.doFilter(request,response);

    }
    private static String decode(String encodedString) {
        return new String(Base64.getUrlDecoder().decode(encodedString));
    }
}
