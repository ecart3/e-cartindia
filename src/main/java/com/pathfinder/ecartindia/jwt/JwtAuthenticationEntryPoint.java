package com.pathfinder.ecartindia.jwt;

import com.pathfinder.ecartindia.WebServiceException;

import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.PrintWriter;

@Slf4j
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
  log.info("authentication enrtypoint---");
//        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//        PrintWriter writer=response.getWriter();
//       writer.println("Access Denied !!"+authException.getMessage());
//      //  response.sendError(401,"UNAUTHORIZED");
//       response.setContentType("application/json");

        int statusCode = HttpServletResponse.SC_UNAUTHORIZED;
        String errorMessage = "Access Denied!";

        // Customize response based on exception type
//        if (authException instanceof ExpiredJwtException) {
//            statusCode = HttpServletResponse.SC_UNAUTHORIZED;
//            errorMessage = "Token expired. Please log in again.";
//}
         if (authException instanceof BadCredentialsException) {
            statusCode = HttpServletResponse.SC_UNAUTHORIZED;
            errorMessage = "Invalid username or password.";
        } else if (authException instanceof LockedException) {
            statusCode = HttpServletResponse.SC_FORBIDDEN;
            errorMessage = "Your account is locked. Please contact support.";
        }
         else {
             statusCode = HttpServletResponse.SC_UNAUTHORIZED;
            errorMessage = "Token expired. Please log in again.";
         }

        // Set the HTTP status code
        response.setStatus(statusCode);

        // Set the content type of the response
        response.setContentType("application/json");

        // Write error message to the response body
        PrintWriter writer = response.getWriter();
        writer.println("{ \"error\": \"Unauthorized\", \"message\": \"" + errorMessage + "\" }");
        writer.flush();

    }
}
