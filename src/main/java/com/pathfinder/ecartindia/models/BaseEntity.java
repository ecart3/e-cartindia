package com.pathfinder.ecartindia.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pathfinder.ecartindia.utility.dto.ActorEntity;
import com.pathfinder.ecartindia.utility.dto.CommonUtils;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value={"createdOn","updatedOn"},allowGetters = true)
@Data
public class BaseEntity {

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(name = "created_on", nullable = false, updatable = false)
    private Date createdOn;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    @Column(name="updated_on", nullable = false)
    @JsonIgnore
    private Date updatedOn;

    @Embedded
    @CreatedBy
    //@Setter(AccessLevel.PRIVATE)
@AttributeOverrides({
@AttributeOverride(name="userEmailValue", column = @Column(name="created_by", updatable = false, nullable = false))
})
    private ActorEntity createdBy;

    @Embedded
    @LastModifiedBy
    @AttributeOverrides({
            @AttributeOverride(name="userEmailValue",  column = @Column(name="updated_by", nullable = false))
    })
    private  ActorEntity updatedBy;

    @PrePersist
    public void prePersist(){
        this.createdOn = new Date();
        this.updatedOn = new Date();
        if(createdBy==null) {
            ActorEntity actor = new ActorEntity(CommonUtils.getCurrentUserName());
            this.createdBy = actor;
            this.updatedBy = actor;
        }
        if(updatedBy==null){
            ActorEntity actor = new ActorEntity(CommonUtils.getCurrentUserName());
            this.updatedOn = new Date();
            this.updatedBy = actor;
        }
    }
    @PreUpdate
    public void preUpdate() {
        if (createdBy == null){
            ActorEntity actor = new ActorEntity(CommonUtils.getCurrentUserName());
            this.updatedOn = new Date();
            this.updatedBy = actor;
    }
   if(updatedBy==null){
    ActorEntity actor = new ActorEntity(CommonUtils.getCurrentUserName());
    this.updatedBy=actor;

}
    }
}
