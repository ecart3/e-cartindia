package com.pathfinder.ecartindia.models;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
//@Table(name="cart")
//@Data
public class Cart extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(fetch =FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private Users user;


    @OneToMany(mappedBy = "cart", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<CartItem> cartItems;

//    @OneToMany
//    private Orders order;

}
