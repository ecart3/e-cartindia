package com.pathfinder.ecartindia.models;

import jakarta.persistence.Embeddable;
import lombok.Data;

@Data
@Embeddable
public class Address {

    private String city;
    private String street;
    private String state;
    private String postalCode;
    private String country;
}
