package com.pathfinder.ecartindia.models;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;

@Entity
@Data
@Table(name="products")
public class Product extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long pId;
    @Column(name="product_name")
    private String name;
    @Column(name="product_desc")
    private String description;
    @Column(name="product_price")
    private Long price;
    @Column(name="stock_quantity")
    private Long stockQuantity;
    @Column(name = "is_live")
    private Boolean isLive;
    @Column(name="brand_name")
    private String brand;
    @Column(name="product_availability")
    private Boolean avaibility;
    @Column(name = "image_name")
    private String imageName;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="category_id")
    private Category category;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "user_id")
//    private Users users;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "cart_id")
//    private Cart cart;

}
