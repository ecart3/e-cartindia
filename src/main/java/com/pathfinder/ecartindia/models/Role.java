package com.pathfinder.ecartindia.models;

import com.pathfinder.ecartindia.enums.ERole;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.Set;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "role")
public class Role extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    @Column(name="role_name")
    private ERole name;
    @Column(name="description")
    private String discritption;

   // @ManyToMany(mappedBy = "roles", fetch = FetchType.EAGER)
  //  private Set<Users> users;

}
