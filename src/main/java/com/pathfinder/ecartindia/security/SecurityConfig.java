package com.pathfinder.ecartindia.security;


import com.pathfinder.ecartindia.WebServiceException;
import com.pathfinder.ecartindia.jwt.JwtAuthenticationEntryPoint;
import com.pathfinder.ecartindia.jwt.JwtAuthenticationFilter;
import com.pathfinder.ecartindia.jwt.JwtHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;

import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static jakarta.servlet.DispatcherType.ERROR;
import static jakarta.servlet.DispatcherType.FORWARD;


@Configuration
@EnableWebSecurity
public class SecurityConfig  {

    @Autowired
    JwtAuthenticationFilter jwtAuthenticationFilter;
    @Autowired
    JwtAuthenticationEntryPoint entryPoint;

    @Bean
    public UserDetailsService userDetailsService() {
//        System.out.println("userDetails Servicse");
        return new UserInfo();
//   BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//   encoder.encode("monu");
//            UserDetails user = User.withUsername("user")
//                    .password(encoder.encode("user"))
//                    .roles("USER")
//                    .build();
////
////
//
//            UserDetails admin = User.withUsername("admin")
//                    .password(encoder.encode("admin"))
//                    .roles("ADMIN")
//                    .build();
//            return new InMemoryUserDetailsManager(user, admin);
        }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        System.out.println("Security Filter chain");
//    return http.csrf().disable()
//            .authorizeHttpRequests()
//            .requestMatchers("/auth/**").permitAll()
//            .requestMatchers("/products/**").hasRole("ADMIN")
//            .requestMatchers("/category/**").hasRole("USER")
//            .anyRequest()
//            .authenticated()
//            .and()
//            .sessionManagement()
//            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//            //.maximumSessions(2).maxSessionsPreventsLogin(true)
//            .and()
//            .authenticationProvider(authenticationProvider())
//            .exceptionHandling(ex->ex.authenticationEntryPoint(entryPoint))
//            .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
//            .build();
//    }
        http.csrf(csrf->csrf.disable())
                .cors(cors->cors.disable())
                .authorizeHttpRequests(
                        auth->
                                auth.requestMatchers("/auth/**","/user/**").permitAll()
                                        .requestMatchers("/products/**").hasRole("ADMIN")
                                        .requestMatchers("/category/**").hasRole("USER")
                                        .anyRequest().authenticated())
                .exceptionHandling(ex->ex.authenticationEntryPoint(entryPoint))
                .sessionManagement(session->session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        http.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }


    @Bean
    public AuthenticationProvider authenticationProvider() {
       System.out.println("authentication provider");
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService());
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }
    // Password Encoding
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
