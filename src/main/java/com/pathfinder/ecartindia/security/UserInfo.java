package com.pathfinder.ecartindia.security;

import com.pathfinder.ecartindia.WebServiceException;
import com.pathfinder.ecartindia.models.Users;
import com.pathfinder.ecartindia.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import  org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Objects;
import java.util.Optional;

@Service
public class UserInfo implements UserDetailsService {

    @Autowired
    public UserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("-------------------Prasanjeet loadUserbyUsername");
    Users user=userRepository.findByuserName(username).get();
    if(!ObjectUtils.isEmpty(user)){
        return new UserInfoDetails(user);
    }
    else {
        throw new WebServiceException("Username is not found");
    }

    }
}
